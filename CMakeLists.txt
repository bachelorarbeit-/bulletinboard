cmake_minimum_required(VERSION 3.10)

project(mk_tfhe_voting_bulletinboard)
set(CMAKE_CXX_STANDARD 17)

if (no_tests)
	add_subdirectory(src)
	add_subdirectory(server)
else ()
	add_subdirectory(src)
	add_subdirectory(server)
	add_subdirectory(test)
endif ()
