#include "rest/BulletinBoardServer.h"
#include "rest/CoordinatorConnector.h"

#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <bulletinboard/BulletinBoard.h>
#include <mk-tfhe-voting/helpers/SaveEnv.h>

#include <thread>
#include <chrono>

namespace server {

	struct RestServer {
		std::unique_ptr<server::BulletinBoardServer> bulletinboard;
		std::shared_ptr<CoordinatorConnector> bbclient;

		log4cplus::Logger logger{logger::getDefaultLogger()};

		void startBulletinBoard(web::uri const &listenUri, web::uri const &externalUri, web::uri const &coordinatorUri) {
			bbclient = std::make_shared<CoordinatorConnector>(coordinatorUri, externalUri);
			auto bb = std::make_shared<hsr::bulletinboard::BulletinBoard>(bbclient);
			bulletinboard = std::make_unique<server::BulletinBoardServer>(listenUri.to_string(), bb);
			bulletinboard->open().wait();
			LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT(std::string{"Listening for requests at: "} + listenUri.to_string()));
		}

	public:
		RestServer(utility::string_t const &listenAddress, utility::string_t const &externalAddress) {
			auto coordinatorUri = web::uri_builder{U(getSaveEnv("COORDINATOR_URI", "http://localhost:8080"))}.to_uri();

			web::uri_builder listenUri{listenAddress};
			web::uri_builder externalUri{externalAddress};
			startBulletinBoard(listenUri.to_uri(), externalUri.to_uri(), coordinatorUri);
		}

		void stop() {
			bulletinboard->close().wait();
		}
	};
}

int main() {
	log4cplus::Logger logger{logger::getDefaultLogger()};
	utility::string_t listenAddress = U(std::string{"http://"} + getSaveEnv("SELF_HOSTNAME", "localhost") + ":" + getSaveEnv("SELF_PORT", "42144"));
	utility::string_t externalAddress = U(std::string{"http://"} + getSaveEnv("EXTERNAL_HOSTNAME", "localhost") + ":" + getSaveEnv("EXTERNAL_PORT", "42144"));

	server::RestServer restServer{listenAddress, externalAddress};

	LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("Enter quit to terminate"));
	std::string line;
	while (line != "quit") {
		std::this_thread::sleep_for(std::chrono::seconds(5));
		std::getline(std::cin, line);
	}

	restServer.stop();
}
