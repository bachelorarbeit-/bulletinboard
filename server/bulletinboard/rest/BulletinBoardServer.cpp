#include "BulletinBoardServer.h"
#include "JsonConversion.h"
#include <iostream>
#include <tfhe_core.h>
#include <mkTFHEsamples.h>
#include <mk-tfhe-voting/exceptions/JsonError.h>

namespace server {
	using namespace web::http;

	BulletinBoardServer::BulletinBoardServer(utility::string_t url, std::shared_ptr<hsr::bulletinboard::BulletinBoard> bulletinboard)
			: m_listener{url},
			  bulletinboard{bulletinboard} {
		m_listener.support(methods::GET, std::bind(&BulletinBoardServer::handle_get, this, std::placeholders::_1));
		m_listener.support(methods::POST, std::bind(&BulletinBoardServer::handle_post, this, std::placeholders::_1));
	}

	void BulletinBoardServer::handle_get(http_request message) {
		try {
			auto pathString{web::http::uri::decode(message.relative_uri().path())};
			auto paths = web::http::uri::split_path(pathString);
			if (!paths.empty()) {
				if (paths.at(0) == "heartbeat") {
					message.reply(status_codes::OK);
					return;
				} else if (paths.at(0) == "state") {
					try {
						if (paths.size() > 1) {
							std::vector<hsr::data::model::Vote> votes{bulletinboard->getVotesByHash(paths.at(1))};
							auto stateJson{cotesFromVector(votes)};
							message.reply(status_codes::OK, stateJson);
							return;
						} else {
							std::vector<hsr::data::model::Vote> votes{bulletinboard->getVotes()};
							auto stateJson{cotesFromVector(votes)};
							message.reply(status_codes::OK, stateJson);
							return;
						}
					} catch (std::exception &exception) {
						LOG4CPLUS_ERROR(logger, exception.what());
						message.reply(status_codes::InternalError);
						return;
					}
				}
			}
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{"Not implemented GET "} + pathString));
			message.reply(status_codes::NotFound);
		} catch (const std::exception &e) {
			LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT(std::string{"unhandled Exception: "} + std::string{e.what()}));
			throw e;
		}
	}

	void BulletinBoardServer::handle_post(http_request message) {
		try {
			auto pathString{web::http::uri::decode(message.relative_uri().path())};
			auto paths = web::http::uri::split_path(pathString);
			if (!paths.empty()) {
				if (paths.at(0) == "generatekeys") {
					message.reply(status_codes::OK);
					bulletinboard->generateKeys();
					return;

				} else if (paths.at(0) == "votedone") {
					message.reply(status_codes::OK);
					bulletinboard->voteDone();
					return;

				} else if (paths.at(0) == "vote") {
					std::vector<std::shared_ptr<MKLweSample>> ballot{};
					unsigned ballotId{};
					try {
						auto voteJson{message.extract_json().get()};
						getBallotFromJson(voteJson, bulletinboard->getParams(), ballot, ballotId);
					} catch (const web::json::json_exception &exception) {
						message.reply(status_codes::BadRequest);
						throw JsonError("parsing ballot failed", exception);
					}
					message.reply(status_codes::OK);
					bulletinboard->submitVote(ballotId, ballot);
					return;

				} else if (paths.at(0) == "sendpartialresult") {
					try {
						auto json{message.extract_json().get()};
						auto phases{phasesFromJson(json)};
					} catch (const web::json::json_exception &exception) {
						message.reply(status_codes::BadRequest);
						throw JsonError("parsing result failed", exception);
					}
					message.reply(status_codes::OK);
					// TODO publish results
					return;
				}
			}
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{"Not implemented POST "} + pathString));
			message.reply(status_codes::NotFound);
		} catch (const std::exception &e) {
			LOG4CPLUS_ERROR(logger, LOG4CPLUS_TEXT(std::string{"unhandled Exception: "} + std::string{e.what()}));
			throw e;
		}
	}
}