#ifndef MK_TFHE_VOTING_BULLETINBOARDSERVER_H
#define MK_TFHE_VOTING_BULLETINBOARDSERVER_H


#include "bulletinboard/BulletinBoard.h"
#include "stdafx.h"
#include <memory>

namespace server {
	using http_request = web::http::http_request;
	using listener = web::http::experimental::listener::http_listener;

	class BulletinBoardServer {
		void handle_get(http_request message);

		void handle_post(http_request message);

		listener m_listener;
		std::shared_ptr<hsr::bulletinboard::BulletinBoard> bulletinboard;
		log4cplus::Logger logger{logger::getDefaultLogger()};
	public:
		BulletinBoardServer() = default;

		BulletinBoardServer(utility::string_t url, std::shared_ptr<hsr::bulletinboard::BulletinBoard> bulletinboard);

		pplx::task<void> open() { return m_listener.open(); }

		pplx::task<void> close() { return m_listener.close(); }
	};
}


#endif //MK_TFHE_VOTING_BULLETINBOARDSERVER_H
