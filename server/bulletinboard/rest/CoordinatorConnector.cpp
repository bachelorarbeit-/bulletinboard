#include "CoordinatorConnector.h"
#include "TrusteeConnector.h"
#include "ParamsFromJson.h"
#include "JsonConversion.h"

#include <mk-tfhe-voting/exceptions/CoordinatorError.h>
#include <mk-tfhe-voting/exceptions/JsonError.h>
#include <thread>
#include <chrono>

CoordinatorConnector::CoordinatorConnector(const web::uri &coordinatorUri, web::uri selfUri)
		: client{coordinatorUri}, self{std::move(selfUri)} {}

web::http::http_response CoordinatorConnector::tryRequest(const web::http::http_request &httpRequest) {
	using namespace std::chrono_literals;
	web::http::http_response httpResponse;

	bool isMaxDelayReached{false};
	unsigned int maxDelay{12};
	unsigned int unsuccessfulResponse{1};
	unsigned int multipleDelay{1};

	while (true) {
		try {
			httpResponse = client.request(httpRequest).get();
			break;
		} catch (web::http::http_exception const &exception) {
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{exception.what()} + " " + client.base_uri().to_string() + httpRequest.request_uri().to_string()));
			if (!isMaxDelayReached) {
				if (unsuccessfulResponse % 12 == 0) {
					++multipleDelay;
					if (multipleDelay > maxDelay) {
						isMaxDelayReached = true;
					}
				}
				++unsuccessfulResponse;
			} else {
				multipleDelay = maxDelay;
			}
			std::this_thread::sleep_for(5s * multipleDelay);
		}
	}
	checkResponse(httpResponse);
	return httpResponse;
}

void CoordinatorConnector::checkResponse(const web::http::http_response &httpResponse) {
	if (httpResponse.status_code() != web::http::status_codes::OK) {
		throw CoordinatorError("Coordinator status code " + std::to_string(httpResponse.status_code()));
	}
}

web::http::http_response CoordinatorConnector::getRequest(const web::uri &uri) {
	web::http::http_request httpRequest{web::http::methods::GET};
	httpRequest.headers().add("Authorization", apiKey);
	httpRequest.set_request_uri(uri);
	return tryRequest(httpRequest);
}

web::http::http_response CoordinatorConnector::postRequest(const web::uri &uri, const web::json::value &body) {
	web::http::http_request httpRequest{web::http::methods::POST};
	httpRequest.headers().add("Authorization", apiKey);
	httpRequest.set_request_uri(uri);
	httpRequest.set_body(body);
	return tryRequest(httpRequest);
}

void CoordinatorConnector::registerBulletinBoardToCoordinator() {
	web::json::value body = web::json::value::object();
	body["hostname"] = web::json::value::string(self.host());
	body["port"] = web::json::value::number(self.port());
	try {
		auto json = postRequest("/bulletinBoard/register", body).extract_json().get();
		apiKey = "Bearer " + json.at("apiKey").as_string();
		bulletinBoardId = json.at("id").as_number().to_uint32();
	} catch (web::json::json_exception const &exception) {
		throw JsonError("parsing register response failed", exception);
	}
}

std::vector<VotesText> CoordinatorConnector::getVotes() {
	try {
		auto json = postRequest("/bulletinBoard/getVotes", web::json::value::object()).extract_json().get();
		std::vector<VotesText> votesInfo{};
		for (auto &vote : json.as_array()) {
			VotesText votesText{};
			votesText.question = vote.at("question").as_string();
			votesInfo.push_back(votesText);
		}
		return votesInfo;
	} catch (web::json::json_exception const &exception) {
		throw JsonError("parsing votes text failed", exception);
	}
}

std::shared_ptr<BulletinBoardParams> CoordinatorConnector::registerBulletinBoardToVote(unsigned voteNumber) {
	web::json::value body = web::json::value::object();
	try {
		auto json = postRequest("/bulletinBoard/registerToVote/" + std::to_string(voteNumber), body).extract_json().get();
		Params params{};
		std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey{};
		unsigned int voteId{};
		voteId = json.at("id").as_number().to_uint32();

		params = getParamsFromJson(json.at("voteParams"));
		for (auto &trustee : json.at("trustees").as_array()) {
			web::uri_builder uriBuilder{};
			uriBuilder.set_host(trustee.at("trustee").at("hostname").as_string());
			uriBuilder.set_port(trustee.at("trustee").at("port").as_number().to_uint32());
			trusteeConnectors.emplace_back(std::make_shared<TrusteeConnector>(uriBuilder.to_uri()));
		}
		for (auto rLwePubKeyJson : json.at("rLwePubKey").as_array()) {
			auto torusPolynomial = std::make_shared<TorusPolynomial>(params.N);
			for (unsigned int i = 0; i < static_cast<unsigned int>(params.N); i++) {
				torusPolynomial->coefsT[i] = rLwePubKeyJson.at("coefsT").as_array().at(i).as_number().to_int32();
			}
			rLwePubKey.push_back(torusPolynomial);
		}
		std::vector<std::weak_ptr<ITrusteeForBulletinBoard>> trustees{trusteeConnectors.begin(), trusteeConnectors.end()};
		return std::make_shared<BulletinBoardParams>(bulletinBoardId, voteId, trustees, rLwePubKey, params);
	} catch (web::json::json_exception const &exception) {
		throw JsonError("parsing params failed", exception);
	}
}

void CoordinatorConnector::signalReady() {
	getRequest("/bulletinboard/ready");
}

void CoordinatorConnector::trackBallotPaper(unsigned ballotPaperId, std::string const &type) {
	web::json::value body = web::json::value::object();
	body["bulletinBoardId"] = web::json::value::number(bulletinBoardId);
	body["voteId"] = web::json::value::number(ballotPaperId);
	body["type"] = web::json::value::string(type);

	postRequest("/bulletinBoard/trackVote", body);
}

std::vector<unsigned> CoordinatorConnector::checkBallotPaperReceived(unsigned ballotPaperId) {
	try {
		auto json = getRequest("/bulletinBoard/checkVoteState/" + std::to_string(ballotPaperId)).extract_json().get();
		std::vector<unsigned> readyBulletinboards{};
		for (auto &status : json.as_array()) {
			if (status.at("type").as_string() == "voteReceived") {
				readyBulletinboards.push_back(status.at("bulletinboardId").as_number().to_uint32());
			}
		}
		return readyBulletinboards;
	} catch (web::json::json_exception const &exception) {
		throw JsonError("parsing ballot status failed", exception);
	}
}

void CoordinatorConnector::publishCounters(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> vector) {
	web::json::value body = jsonMapFromCounters(bulletinBoardId, vector);
	postRequest("bulletinBoard/counters/" + std::to_string(voteId), body);
}
