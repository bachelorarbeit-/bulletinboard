#ifndef MK_TFHE_VOTING_COORDINATORCONNECTOR_H
#define MK_TFHE_VOTING_COORDINATORCONNECTOR_H


#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include "TrusteeConnector.h"
#include <mk-tfhe-voting/helpers/ICoordinatorForBulletinBoard.h>
#include <cpprest/http_client.h>

class CoordinatorConnector : public ICoordinatorForBulletinBoard {
	web::http::client::http_client client;
	web::uri self;
	std::string apiKey{};
	unsigned bulletinBoardId{};
	log4cplus::Logger logger{logger::getDefaultLogger()};
	std::vector<std::shared_ptr<TrusteeConnector>> trusteeConnectors{};

	web::http::http_response tryRequest(const web::http::http_request &httpRequest);

	web::http::http_response getRequest(const web::uri &uri);

	web::http::http_response postRequest(const web::uri &uri, const web::json::value &body);

	void checkResponse(const web::http::http_response &httpResponse);

public:
	explicit CoordinatorConnector(const web::uri &coordinatorUri, web::uri selfUri);

	void registerBulletinBoardToCoordinator() override;

	std::vector<VotesText> getVotes() override;

	std::shared_ptr<BulletinBoardParams> registerBulletinBoardToVote(unsigned voteNumber) override;

	void signalReady() override;

	void trackBallotPaper(unsigned ballotPaperId, std::string const &type) override;

	std::vector<unsigned> checkBallotPaperReceived(unsigned ballotPaperId) override;

	void publishCounters(unsigned int bulletinBoardId, unsigned int voteId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> vector) override;
};


#endif //MK_TFHE_VOTING_COORDINATORCONNECTOR_H
