#ifndef MK_TFHE_VOTING_JSONCONVERSION_H
#define MK_TFHE_VOTING_JSONCONVERSION_H


#include <cpprest/http_client.h>
#include <mkTFHEsamples.h>
#include <mk-tfhe-voting/helpers/Params.h>
#include <mk-tfhe-voting/helpers/BulletinBoardParams.h>
#include <mk-tfhe-voting/data/model/Vote.h>


static web::json::value jsonFromMKLweSample(std::shared_ptr<MKLweSample> const &sample) {
	web::json::value sampleJson = web::json::value::object();
	for (int i{}; i < sample->n * sample->parties; ++i) {
		sampleJson["phaseShift"][i] = web::json::value::number(sample->a[i]);
	}
	sampleJson["phase"] = web::json::value::number(sample->b);
	return sampleJson;
}

static web::json::value jsonFromCounters(unsigned int bbId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> const &counters) {
	web::json::value countersJson = web::json::value::object();
	countersJson[U("bbId")] = web::json::value::number(bbId);
	countersJson[U("choices")] = web::json::value::number(counters.size());
	for (unsigned i{}; i < counters.size(); ++i) {
		web::json::value counterJson = web::json::value::object();
		for (unsigned j{}; j < counters.at(i).size(); j++) {
			counterJson[U("counterBits")][j] = jsonFromMKLweSample(counters.at(i).at(j));
		}
		countersJson[U("counters")][i] = counterJson;
	}
	return countersJson;
}

static web::json::value jsonMapFromCounters(unsigned int bbId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> const &counters) {
	web::json::value countersJson = web::json::value::object();
	for (unsigned i{}; i < counters.size(); ++i) {
		web::json::value counterJson = web::json::value::array();
		for (unsigned j{}; j < counters.at(i).size(); j++) {
			counterJson[j] = jsonFromMKLweSample(counters.at(i).at(j));
		}
		countersJson[std::to_string(i + 1)] = counterJson;
	}
	return countersJson;
}

static std::vector<std::vector<Torus32>> phasesFromJson(web::json::value const &json) {
	std::vector<std::vector<Torus32>> counters{};
	for (auto &counterJson : json.as_array()) {
		std::vector<Torus32> counter{};
		for (auto &phase : counterJson.as_array()) {
			counter.push_back(phase.as_number().to_int32());
		}
		counters.push_back(counter);
	}
	return counters;
}

static std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKeyFromJson(web::json::value const &json) {
	std::vector<std::shared_ptr<TorusPolynomial>> rLwePubKey{};
	for (auto &polynomialJson : json.as_array()) {
		int N = polynomialJson.at("N").as_number().to_int32();
		auto torusPolynomial = std::make_shared<TorusPolynomial>(N);
		for (int i{}; i < N; ++i) {
			torusPolynomial->coefsT[i] = polynomialJson.at("coefsT").at(i).as_number().to_int32();
		}
		rLwePubKey.push_back(torusPolynomial);
	}
	return rLwePubKey;
}

static std::vector<std::shared_ptr<MKTGswUESample>> bskFromJson(web::json::value const &json, std::shared_ptr<TLweParams> const &tLweParams, std::shared_ptr<MKTFHEParams> const &mkParams) {
	std::vector<std::shared_ptr<MKTGswUESample>> bsk{};
	for (auto &keybitJson : json.as_array()) {
		auto keybit{std::make_shared<MKTGswUESample>(tLweParams.get(), mkParams.get())};
		keybit->current_variance = keybitJson.at("current_variance").as_number().to_int32();
		keybit->party = keybitJson.at("party").as_number().to_int32();
		for (int j{}; j < 6 * keybit->dg; ++j) {
			int coefTCounter{};
			for (auto &coefsJson : keybitJson.at("c").at(j).at("coefsT").as_array()) {
				keybit->c[j].coefsT[coefTCounter++] = coefsJson.as_number().to_int32();
			}
		}
		bsk.push_back(keybit);
	}
	return bsk;
}

static void fillKsFromJson(LweKeySwitchKey *ks, web::json::value const &ksJson) {
	ks->n = ksJson.at("n").as_number().to_int32();
	ks->t = ksJson.at("t").as_number().to_int32();
	ks->basebit = ksJson.at("basebit").as_number().to_int32();
	ks->base = ksJson.at("base").as_number().to_int32();
	for (int i{}; i < ks->n * ks->t * ks->base; ++i) {
		ks->ks0_raw[i].b += ksJson.at("ks").at(i).at("phase").as_number().to_int32();
		ks->ks0_raw[i].current_variance = ksJson.at("ks").at(i).at("current_variance").as_number().to_int32();
		for (int j{}; j < ks->out_params->n; ++j) {
			auto &coef = ksJson.at("ks").at(i).at("phaseShift").as_array();
			ks->ks0_raw[i].a[j] = coef.at(j).as_number().to_int32();
		}
		for (int32_t p = 0; p < ks->n * ks->t; ++p)
			ks->ks1_raw[p] = ks->ks0_raw + ks->base * p;
		for (int32_t p = 0; p < ks->n; ++p)
			ks->ks[p] = ks->ks1_raw + ks->t * p;
	}
}

static void getBallotFromJson(web::json::value &voteJson, const std::shared_ptr<BulletinBoardParams> &params, std::vector<std::shared_ptr<MKLweSample>> &ballot, unsigned int &ballotId) {
	ballotId = voteJson.at("ballotId").as_number().to_uint32();
	int np = params->mkParams->parties * params->mkParams->n;
	for (auto &sample : voteJson.at("ballot").as_array()) {
		ballot.emplace_back(std::make_shared<MKLweSample>(params->lwePubKeyParams.get(), params->mkParams.get()));
		for (int j{}; j < np; ++j) {
			ballot.back()->a[j] = sample.at("phaseShift")[j].as_number().to_int32();
		}
		ballot.back()->b = sample.at("phase").as_number().to_int32();
	}
}

static web::json::value cotesFromVector(const std::vector<hsr::data::model::Vote> &votes) {
	web::json::value votesJson = web::json::value::array();
	for (unsigned int i{0}; i < votes.size(); ++i) {
		votesJson[i] = web::json::value::object();
		votesJson[i][U("id")] = web::json::value::number(votes.at(i).getExternalId());
		votesJson[i][U("ready")] = web::json::value::boolean(votes.at(i).isReady());
		votesJson[i][U("processed")] = web::json::value::boolean(votes.at(i).isProcessed());
		votesJson[i][U("hash")] = web::json::value::string(votes.at(i).getHash());
	}
	return votesJson;
}

#endif //MK_TFHE_VOTING_JSONCONVERSION_H
