#include "TrusteeConnector.h"
#include "JsonConversion.h"

#include <bulletinboard/exceptions/TrusteeError.h>
#include <mk-tfhe-voting/exceptions/JsonError.h>
#include <thread>
#include <chrono>

TrusteeConnector::TrusteeConnector(const web::uri &base_uri) : client{base_uri} {}

web::http::http_response TrusteeConnector::tryRequest(const web::http::http_request &httpRequest) {
	using namespace std::chrono_literals;
	web::http::http_response httpResponse;
	while (true) {
		try {
			httpResponse = client.request(httpRequest).get();
			break;
		} catch (const web::http::http_exception &exception) {
			LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT(std::string{exception.what()} + " " + client.base_uri().to_string() + httpRequest.request_uri().to_string()));
			std::this_thread::sleep_for(5s);
		}
	}
	checkResponse(httpResponse);
	return httpResponse;
}

void TrusteeConnector::checkResponse(const web::http::http_response &httpResponse) {
	if (httpResponse.status_code() != web::http::status_codes::OK) {
		throw TrusteeError("trustee status code " + std::to_string(httpResponse.status_code()));
	}
}

web::http::http_response TrusteeConnector::getRequest(const web::uri &uri) {
	web::http::http_request httpRequest{web::http::methods::GET};
	httpRequest.set_request_uri(uri);
	return tryRequest(httpRequest);
}

web::http::http_response TrusteeConnector::postRequest(const web::uri &uri, const web::json::value &body) {
	web::http::http_request httpRequest{web::http::methods::POST};
	httpRequest.set_request_uri(uri);
	httpRequest.set_body(body);
	return tryRequest(httpRequest);
}

KeySwitchingKeys TrusteeConnector::getKeys(LweKeySwitchKey *ks, std::shared_ptr<TLweParams> const &tLweParams, std::shared_ptr<MKTFHEParams> const &mkParams) {
	try {
		auto json = getRequest(U("getkeys")).extract_json().get();
		KeySwitchingKeys keys{};
		keys.bootstrappingKey = bskFromJson(json.at("bsk"), tLweParams, mkParams);
		keys.rLwePubKey = rLwePubKeyFromJson(json.at("rLwePubKey"));
		fillKsFromJson(ks, json.at("ksk"));
		return keys;
	} catch (const web::json::json_exception &exception) {
		throw JsonError("parsing keys failed", exception);
	}
}

void TrusteeConnector::sendEncryptedCounters(unsigned int bulletinboardId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters) {
	web::json::value body = jsonFromCounters(bulletinboardId, encryptedCounters);
	postRequest(U("sendcounters"), body);
}
