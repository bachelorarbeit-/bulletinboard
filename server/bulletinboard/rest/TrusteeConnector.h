#ifndef MK_TFHE_VOTING_TRUSTEECONNECTOR_H
#define MK_TFHE_VOTING_TRUSTEECONNECTOR_H


#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <mk-tfhe-voting/helpers/ITrusteeForBulletinBoard.h>
#include <cpprest/http_client.h>

class TrusteeConnector : public ITrusteeForBulletinBoard {
	web::http::client::http_client client;
	log4cplus::Logger logger{logger::getDefaultLogger()};

	web::http::http_response tryRequest(const web::http::http_request &httpRequest);

	web::http::http_response getRequest(const web::uri &uri);

	web::http::http_response postRequest(const web::uri &uri, const web::json::value &body);

	void checkResponse(const web::http::http_response &httpResponse);

public:
	explicit TrusteeConnector(const web::uri &base_uri);

	KeySwitchingKeys getKeys(LweKeySwitchKey *ks, std::shared_ptr<TLweParams> const &tLweParams, std::shared_ptr<MKTFHEParams> const &mkParams) override;

	void sendEncryptedCounters(unsigned int bulletinboardId, std::vector<std::vector<std::shared_ptr<MKLweSample>>> encryptedCounters) override;
};


#endif //MK_TFHE_VOTING_TRUSTEECONNECTOR_H
