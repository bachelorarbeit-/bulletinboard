#include "BulletinBoard.h"
#include <mk-tfhe-voting/helpers/KeySwitchingKeys.h>
#include <mk-tfhe-voting/helpers/CyclicBarrier.h>
#include <mk-tfhe-voting/helpers/SaveEnv.h>

#include <mkTFHEkeygen.h>
#include <mkTFHEfunctions.h>
#include <stdexcept>
#include <cmath>
#include <optional>
#include <chrono>
#include <thread>
#include <iostream>
#include <execution>
#include <mk-tfhe-voting/exceptions/DataException.h>
#include <mk-tfhe-voting/exceptions/BulletinBoardError.h>

namespace hsr::bulletinboard {
	static void myMKTGswExpand(MKTGswExpSample *result, const std::shared_ptr<MKTGswUESample> sample, const std::vector<std::shared_ptr<TorusPolynomial>> &rlwePubKey, const MKTFHEParams *mkParams) {
		const int32_t N = mkParams->N;
		const int32_t dg = mkParams->dg;
		const int32_t party = sample->party;
		const int32_t parties = mkParams->parties;

		// C = (0, ..., d1, ..., 0, c1, d0, ..., d0, ..., d0, c0)
		for (int j = 0; j < dg; ++j) {
			for (int i = 0; i <= parties; ++i) {
				torusPolynomialClearN(&result->y[i * dg + j], N);
			}

			torusPolynomialCopyN(&result->y[party * dg + j], &sample->d[dg + j], N);
			torusPolynomialCopyN(&result->c1[j], &sample->c[dg + j], N);

			for (int i = 0; i < parties; ++i) {
				torusPolynomialCopyN(&result->x[i * dg + j], &sample->d[j], N);
			}

			// c0 in place c0
			torusPolynomialCopyN(&result->c0[j], &sample->c[j], N);
		}

		TorusPolynomial *X = new_TorusPolynomial(N);
		TorusPolynomial *Y = new_TorusPolynomial(N);
		TorusPolynomial *b_temp = new_TorusPolynomial(N);
		IntPolynomial *u = new_IntPolynomial_array(dg, N);

		for (int i = 0; i < party; ++i) {
			for (int j = 0; j < dg; ++j) {
				// b_temp = b_i[j] - b_party[j] = Pkey[i*gDimension + j] - Pkey[party*gDimension + j]
				torusPolynomialSubN(b_temp, &(*rlwePubKey.at(i * dg + j)), &(*rlwePubKey.at(party * dg + j)), N);
				// g^{-1}(b_temp) = [u_0, ...,u_dg-1] intPolynomials
				MKtGswTorus32PolynomialDecompGassembly(u, b_temp, mkParams);

				// X=0 and Y=0
				torusPolynomialClearN(X, N);
				torusPolynomialClearN(Y, N);
				for (int l = 0; l < dg; ++l) {
					// X = xi[j] = <g^{-1}(b_temp), f0>
					torusPolynomialAddMulRFFTN(X, &u[l], &sample->f[l], N);
					// Y = yi[j] = <g^{-1}(b_temp), f1>
					torusPolynomialAddMulRFFTN(Y, &u[l], &sample->f[dg + l], N);
				}

				// xi = d0 + xi
				torusPolynomialAddTo1(&result->x[i * dg + j], X);
				// yi = 0 + yi
				torusPolynomialAddTo1(&result->y[i * dg + j], Y);
			}
		}

		for (int i = party + 1; i < parties; ++i) {
			for (int j = 0; j < dg; ++j) {
				// b_temp = b_i[j] - b_party[j] = Pkey[i*gDimension + j] - Pkey[party*gDimension + j]
				torusPolynomialSubN(b_temp, &(*rlwePubKey.at(i * dg + j)), &(*rlwePubKey.at(party * dg + j)), N);
				// g^{-1}(b_temp) = [u_0, ...,u_dg-1] intPolynomials
				MKtGswTorus32PolynomialDecompGassembly(u, b_temp, mkParams);

				// X=0 and Y=0
				torusPolynomialClearN(X, N);
				torusPolynomialClearN(Y, N);
				for (int l = 0; l < dg; ++l) {
					// X = xi[j] = <g^{-1}(b_temp), f0>
					torusPolynomialAddMulRFFTN(X, &u[l], &sample->f[l], N);
					// Y = yi[j] = <g^{-1}(b_temp), f1>
					torusPolynomialAddMulRFFTN(Y, &u[l], &sample->f[dg + l], N);
				}

				// xi = d0 + xi
				torusPolynomialAddTo1(&result->x[i * dg + j], X);
				// yi = 0 + yi
				torusPolynomialAddTo1(&result->y[i * dg + j], Y);
			}
		}

		result->party = sample->party;
		// TODO: fix this
		result->current_variance = sample->current_variance;

		delete_TorusPolynomial(X);
		delete_TorusPolynomial(Y);
		delete_TorusPolynomial(b_temp);
		delete_IntPolynomial_array(dg, u);
	}

	std::shared_ptr<BulletinBoardParams> BulletinBoard::paramsFromCoordinator() {
		auto coord{this->coordinator.lock()};
		coord->registerBulletinBoardToCoordinator();
		coord->getVotes();
		unsigned voteId{static_cast<unsigned int>(stol(getSaveEnv("VOTE_ID", "1")))};
		return coord->registerBulletinBoardToVote(voteId);
	}


	BulletinBoard::BulletinBoard(std::weak_ptr<ICoordinatorForBulletinBoard> coordinator) : coordinator{std::move(coordinator)}, params{paramsFromCoordinator()}, bsKey{getPtr()}, bsKeyFft{generateBsKeyFft()}, fft{true},
																							repo{std::make_shared<data::DataRepository>(std::string{"BulletinBoard"})}, countermutex{}, readyStatemutex{}, shouldStopReaydCheck{false},
																							shouldStopCalc{false}, started{false} {
		initCounters();
	}

	BulletinBoard::BulletinBoard(std::weak_ptr<ICoordinatorForBulletinBoard> coordinator, unsigned int id) : coordinator{std::move(coordinator)}, params{paramsFromCoordinator()}, bsKey{getPtr()},
																											 bsKeyFft{generateBsKeyFft()}, fft{true},
																											 repo{std::make_shared<data::DataRepository>(
																													 std::string{"BulletinBoard"} + std::to_string(id))}, countermutex{},
																											 readyStatemutex{}, shouldStopReaydCheck{false}, started{false} {
		initCounters();
	}

	std::shared_ptr<MKLweBootstrappingKeyFFT> BulletinBoard::generateBsKeyFft() const {
		return std::make_shared<MKLweBootstrappingKeyFFT>(params->mkParams.get(), nullptr, nullptr);
	}

	std::shared_ptr<MKLweBootstrappingKey> BulletinBoard::getPtr() const {
		return std::make_shared<MKLweBootstrappingKey>(
				params->mkParams.get(),
				new_MKTGswExpSample_array(params->mkParams->n * params->mkParams->parties,
										  params->tLweParams.get(), params->mkParams.get()),
				new_LweKeySwitchKey_array(params->mkParams->parties, params->mkParams->n_extract,
										  params->mkParams->dks, params->mkParams->Bksbit,
										  params->lweParams.get())
		);
	}

	BulletinBoard::~BulletinBoard() {
		delete_LweKeySwitchKey_array(params->mkParams->parties, bsKey->ks);
		delete_MKTGswExpSample_array(params->mkParams->parties * params->mkParams->n, bsKey->bk);
		if (fft) {
			delete_MKTGswExpSampleFFT_array(params->mkParams->n * params->mkParams->parties,
											bsKeyFft->bkFFT);
		}
		if (!shouldStopReaydCheck.load()) {
			this->shouldStopReaydCheck.store(true);
			if (this->counterWorker.joinable()) {
				this->counterWorker.join();
			}
			if (this->readyStateWorker.joinable()) {
				this->readyStateWorker.join();
			}
		}
	}

	static void noiselessTrivial(MKLweSample &result, const Torus32 mu) {
		for (int i = 0; i < result.n * result.parties; i++) {
			result.a[i] = 0;
		}
		result.b = mu;
		result.current_variance = 0.0;
	}

	static void addTo(MKLweSample *result, const MKLweSample *sample) {
		for (int i = 0; i < sample->n * sample->parties; i++) {
			result->a[i] += sample->a[i];
		}
		result->b += sample->b;
		result->current_variance += sample->current_variance;
	}

	static void subTo(MKLweSample *result, const MKLweSample *sample) {
		for (int i = 0; i < sample->n * sample->parties; i++) {
			result->a[i] -= sample->a[i];
		}
		result->b -= sample->b;
		result->current_variance += sample->current_variance;
	}

	void addMul(MKLweSample &result, MKLweSample const &sample, int32_t p) {
		for (std::size_t i = 0; i < static_cast<unsigned int>(sample.n * sample.parties); ++i) {
			result.a[i] += sample.a[i] * p;
		}
		result.b += sample.b * p;
		result.current_variance += (p * p) * sample.current_variance;
	}

	void BulletinBoard::await(std::optional<CyclicBarrier> &barrier) {
		if (barrier) {
			barrier->await();
		}
	}

	void BulletinBoard::bsNAND(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsNAND(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsNAND(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);
		static const Torus32 offset = modSwitchToTorus32(1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};

		// compute: (0, 1/8) - sample_a - sample_b
		noiselessTrivial(tmp, offset);
		subTo(&tmp, &sample_a);
		subTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsNOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsNOR(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsNOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);
		static const Torus32 offset = modSwitchToTorus32(-1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};

		// compute: (0, -1/8) - sample_a - sample_b
		noiselessTrivial(tmp, offset);
		subTo(&tmp, &sample_a);
		subTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsAND(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsAND(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsAND(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);
		static const Torus32 offset = modSwitchToTorus32(-1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};

		// compute: (0, -1/8) + sample_a + sample_b
		noiselessTrivial(tmp, offset);

		addTo(&tmp, &sample_a);
		addTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsANDNY(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsANDNY(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsANDNY(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);
		static const Torus32 offset = modSwitchToTorus32(-1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};

		// compute: (0, -1/8) - sample_a + sample_b
		noiselessTrivial(tmp, offset);
		subTo(&tmp, &sample_a);
		addTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsANDYN(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsANDYN(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsANDYN(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);
		static const Torus32 offset = modSwitchToTorus32(-1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};

		// compute: (0, -1/8) + sample_a - sample_b
		noiselessTrivial(tmp, offset);
		addTo(&tmp, &sample_a);
		subTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsXOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsXOR(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsXOR(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};
		static const Torus32 xorConst = modSwitchToTorus32(1, 4);
		noiselessTrivial(tmp, xorConst);

		addMul(tmp, sample_a, 2);
		addMul(tmp, sample_b, 2);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bsOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const {
		std::optional<CyclicBarrier> anOptional{std::nullopt};
		this->bsOR(result, sample_a, sample_b, anOptional);
	}

	void BulletinBoard::bsOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const {
		static const Torus32 MU = modSwitchToTorus32(1, 8);

		MKLweSample tmp{params->lweParams.get(), params->mkParams.get()};
		static const Torus32 orConst = modSwitchToTorus32(1, 8);
		noiselessTrivial(tmp, orConst);

		addTo(&tmp, &sample_a);
		addTo(&tmp, &sample_b);

		await(barrier);

		bootstap(result, MU, tmp);
	}

	void BulletinBoard::bootstap(MKLweSample &out, Torus32 MU, const MKLweSample &in) const {
		// if the phase is positive, sample_c is 1/8 else it is -1/8
		if (fft) {
			MKtfhe_bootstrapFFT(&out, bsKeyFft.get(), MU, &in, params->lweParams.get(),
								params->extractedLweParams.get(), params->tLweParams.get(),
								params->mkParams.get(), nullptr);
		} else {
			MKtfhe_bootstrap(&out, bsKey.get(), MU, &in, params->lweParams.get(), params->extractedLweParams.get(),
							 params->tLweParams.get(), params->mkParams.get());
		}
	}

	void BulletinBoard::submitVote(unsigned int id, std::vector<std::shared_ptr<MKLweSample>> vote) {
		if (!this->started.load()) {
			this->started.store(true);
			this->startWorkers();
		}
		auto coord{this->coordinator.lock()};

		repo->persistVote(id, vote);
		coord->trackBallotPaper(id, "voteReceived");
	}


	void BulletinBoard::checkBallotPaperStates() {
		bool shouldStop = false;
		while (!shouldStop) {
			shouldStop = this->shouldStopReaydCheck.load();
			std::vector<unsigned long> ids{repo->getRecivedVoteIds()};
			int changed{0};
			for (unsigned long id : ids) {
				auto coord{this->coordinator.lock()};
				if (coord != nullptr && coord->checkBallotPaperReceived(id).size() == params->bulletinBoards) {
					changed++;
					repo->setVoteReady(id);
				}
			}
			if (changed == 0 && !shouldStop) {
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
		}
		std::atomic_thread_fence(std::memory_order_seq_cst);
		this->shouldStopCalc.store(true);
	}

	void BulletinBoard::processVotes() {
		this->shouldStopCalc.store(false);
		bool shouldStop = false;
		while (!shouldStop) {
			shouldStop = this->shouldStopCalc.load();
			auto votes{repo->getReadyVotes(this->params->lweParams, this->params->mkParams)};
			for (std::pair<const unsigned long, std::vector<std::shared_ptr<MKLweSample>>> &voteData : votes) {
				auto coord{this->coordinator.lock()};
				if (coord != nullptr) {
					unsigned long id = voteData.first;
					std::vector<std::shared_ptr<MKLweSample>> &vote{voteData.second};
					coord->trackBallotPaper(id, "voteDequeued");

					auto expandedVote{expandVote(vote)};
					std::scoped_lock lock{countermutex};
					if (counters.size() != expandedVote.size()) throw std::range_error("expandedVote does not match number of counter");
					if (numberOfVotes++ == nextCounterLength) {
						nextCounterLength = nextCounterLength << 1u;
						enlargeCounters();
					}

					std::transform(std::execution::par_unseq, counters.begin(), counters.end(), expandedVote.begin(), counters.begin(), [this](auto &c, auto const &v) {
						return addVote(c, v);
					});

					repo->setVoteProcessed(id);
					coord->trackBallotPaper(id, "voteProcessed");
				}
			}
			if (votes.size() == 0 && !shouldStop) {
				std::this_thread::sleep_for(std::chrono::seconds(1));
			}
		}
	}

	CounterType &BulletinBoard::addVote(CounterType &counter, std::shared_ptr<MKLweSample> const &vote) {
		CounterType voteCarry;
		voteCarry.push_back(vote);
		voteCarry.push_back(std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get()));

		std::optional<CyclicBarrier> barrier{std::optional<CyclicBarrier>{2}};
		for (unsigned int i{0}; i < counter.size(); ++i) {
			voteCarry.emplace_back(std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get()));
			std::thread t1{[this, i, &counter, &voteCarry, &barrier] {
				bsAND(*voteCarry[i + 1], *counter[i], *voteCarry[i], barrier);
			}};
			bsXOR(*counter[i], *counter[i], *voteCarry[i], barrier);
			t1.join();
		}
		return counter;
	}

	std::vector<CounterType> BulletinBoard::generateSequences(unsigned inBits, unsigned outBits) const {
		std::vector<CounterType> sequences{};
		for (unsigned oBit{}; oBit < outBits; ++oBit) {
			CounterType bitSequence{};
			for (unsigned iBit{}; iBit < inBits; ++iBit) {
				bitSequence.push_back(std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get()));
				std::size_t const segment = outBits / static_cast<unsigned>(std::pow(2, iBit + 1));
				if ((oBit / segment) % 2) {
					noiselessTrivial(*bitSequence.back(), modSwitchToTorus32(-1, 8));
				} else {
					noiselessTrivial(*bitSequence.back(), modSwitchToTorus32(1, 8));
				}
			}
			sequences.push_back(bitSequence);
		}
		return sequences;
	}

	CounterType BulletinBoard::expandVote(const CounterType &vote) const {
		std::size_t const inBits = vote.size();
		std::size_t const outBits{static_cast<std::size_t>(std::pow(2, inBits))};

		std::vector<CounterType> sequences{generateSequences(inBits, outBits)};

		CounterType outdata{};
		for (unsigned int i{}; i < outBits; ++i) {
			outdata.push_back(std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get()));
			noiselessTrivial(*outdata.back().get(), modSwitchToTorus32(1, 8));
		}
		std::transform(std::execution::par_unseq, outdata.begin(), outdata.end(), sequences.begin(), outdata.begin(), [this, &vote](std::shared_ptr<MKLweSample> &outBit, CounterType &sequence) {
			std::transform(std::execution::par_unseq, sequence.begin(), sequence.end(), vote.rbegin(), sequence.begin(),
						   [this](std::shared_ptr<MKLweSample> &sequenceBit, std::shared_ptr<MKLweSample> const &voteBit) {
							   bsXOR(*sequenceBit, *voteBit, *sequenceBit);
							   return sequenceBit;
						   });
			outBit = std::reduce(std::execution::par_unseq, sequence.begin(), sequence.end(), outBit,
								 [this](std::shared_ptr<MKLweSample> const &bit1, std::shared_ptr<MKLweSample> const &bit2) {
									 auto result{std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get())};
									 bsAND(*result, *bit1, *bit2);
									 return result;
								 }
			);
			return outBit;
		});
		unsigned unusedBits{static_cast<unsigned int>(outBits - params->choices)};
		for (unsigned int i{}; i < unusedBits; ++i) {
			bsOR(*outdata[outdata.size() - 2], *outdata[outdata.size() - 2], *outdata.back());
			outdata.pop_back();
		}
		return outdata;
	}

	void BulletinBoard::initCounters() {
		for (uint32_t i{}; i < params->choices; ++i) {
			counters.emplace_back();
		}
		enlargeCounters();
	}

	void BulletinBoard::enlargeCounters() {
		std::for_each(std::execution::par, counters.begin(), counters.end(), [this](auto &counter) {
			counter.push_back(std::make_shared<MKLweSample>(params->lweParams.get(), params->mkParams.get()));
			noiselessTrivial(*counter.back(), modSwitchToTorus32(-1, 8));
		});
	}

	void BulletinBoard::generateKeys() {
		if (static_cast<unsigned int>(params->mkParams->parties) != params->trustees.size()) {
			throw std::runtime_error{"trustee count mismatch"};
		}

		const int32_t n = params->mkParams->n;

		unsigned numberOfTrustees = params->trustees.size();

		std::vector<KeySwitchingKeys> trusteePubKeys{};
		std::vector<std::shared_ptr<TorusPolynomial>> rlwePubKey{};

		// Create KSK
		for (unsigned trusteeId{}; trusteeId < numberOfTrustees; ++trusteeId) {
			LweKeySwitchKey *ks = &bsKey->ks[trusteeId];
			trusteePubKeys.push_back(params->trustees.at(trusteeId).lock()->getKeys(ks, params->tLweParams, params->mkParams));
			rlwePubKey.insert(rlwePubKey.end(), trusteePubKeys.at(trusteeId).rLwePubKey.begin(), trusteePubKeys.at(trusteeId).rLwePubKey.end());
		}
		rlwePubKey.insert(rlwePubKey.end(), params->rlwePubKey.begin(), params->rlwePubKey.end());

		// Create BSK
		for (unsigned trusteeId{}; trusteeId < numberOfTrustees; ++trusteeId) {
			for (int j = 0; j < n; ++j) {
				myMKTGswExpand(&bsKey->bk[trusteeId * n + j], trusteePubKeys.at(trusteeId).bootstrappingKey.at(j), rlwePubKey, params->mkParams.get());
			}
		}

		if (fft) {
			bsKeyFft->ks = bsKey->ks;
			bsKeyFft->bkFFT = new_MKTGswExpSampleFFT_array(n * numberOfTrustees, params->tLweParams.get(), params->mkParams.get(), params->mkParams->stdevBK);

			// convert bk to bkFFT
			int32_t nb_polys = 2 * (numberOfTrustees + 1) * params->mkParams->dg;

			for (unsigned int p = 0; p < numberOfTrustees; p++) {
				for (int i = 0; i < n; ++i) {
					for (int j = 0; j < nb_polys; ++j) {
						TorusPolynomial_ifft(&bsKeyFft->bkFFT[p * n + i].c[j],
											 &bsKey->bk[p * n + i].c[j]);
					}
					bsKeyFft->bkFFT[p * n + i].party = bsKey->bk[p * n + i].party;
				}
			}
		}
		LOG4CPLUS_INFO(logger, LOG4CPLUS_TEXT("keys generated"));
	}

	void BulletinBoard::voteDone() {
		if (!shouldStopReaydCheck.load()) {
			this->shouldStopReaydCheck.store(true);
			this->counterWorker.join();
			this->readyStateWorker.join();
		}
		this->coordinator.lock()->publishCounters(this->params->bulletinboardId, this->params->voteId, this->counters);
		for (auto &trustee : params->trustees) {
			trustee.lock()->sendEncryptedCounters(this->params->bulletinboardId, this->counters);
			std::this_thread::sleep_for(std::chrono::seconds(2));
		}
	}

	std::vector<CounterType> const &BulletinBoard::getCounters() {
		return counters;
	}

	std::shared_ptr<BulletinBoardParams> const &BulletinBoard::getParams() {
		return params;
	}

	void BulletinBoard::sendPartialResults(std::vector<std::vector<Torus32>>) {

	}

	void BulletinBoard::startWorkers() {
		std::scoped_lock lock(countermutex, readyStatemutex);
		counterWorker = std::thread{[this] {
			processVotes();
		}};
		readyStateWorker = std::thread{[this] {
			checkBallotPaperStates();
		}};
	}

	std::vector<hsr::data::model::Vote> BulletinBoard::getVotes() {
		try {
			return this->repo->findAllVotes();
		} catch (DataException &exception) {
			throw BulletinBoardError("error getting data", exception);
		}
	}

	std::vector<hsr::data::model::Vote> BulletinBoard::getVotesByHash(std::string hash) {
		try {
			return this->repo->findVotesByHash(hash);
		} catch (DataException &exception) {
			throw BulletinBoardError("error getting data", exception);
		}
	}
}