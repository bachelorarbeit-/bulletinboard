#ifndef TELLER_H_
#define TELLER_H_

#include <mk-tfhe-voting/helpers/IBulletinBoardForCoordinator.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForVoter.h>
#include <mk-tfhe-voting/helpers/IBulletinBoardForTrustee.h>
#include <mk-tfhe-voting/helpers/ICoordinatorForBulletinBoard.h>
#include <mk-tfhe-voting/helpers/ITrusteeForBulletinBoard.h>

#include <mk-tfhe-voting/data/DataRepository.h>
#include <mk-tfhe-voting/helpers/BulletinBoardParams.h>
#include <mk-tfhe-voting/helpers/CyclicBarrier.h>

#include <mk-tfhe-voting/logging/LoggerFactory.h>
#include <log4cplus/logger.h>

#include <tfhe.h>
#include <lweparams.h>
#include <lwekey.h>
#include <tlwe.h>
#include <mkTFHEparams.h>
#include <mkTFHEsamples.h>
#include <mkTFHEkeys.h>
#include <memory>
#include <optional>
#include <queue>
#include <tuple>
#include <thread>
#include <atomic>

namespace hsr::bulletinboard {
	using CounterType = std::vector<std::shared_ptr<MKLweSample>>;

	class BulletinBoard : public IBulletinBoardForCoordinator, public IBulletinBoardForVoter, public IBulletinBoardForTrustee {

		std::weak_ptr<ICoordinatorForBulletinBoard> coordinator;
		std::shared_ptr<BulletinBoardParams> params;

		std::shared_ptr<MKLweBootstrappingKey> bsKey;
		std::shared_ptr<MKLweBootstrappingKeyFFT> bsKeyFft;
		bool fft;

		std::size_t nextCounterLength{1};
		std::size_t numberOfVotes{};
		std::vector<CounterType> counters;

		std::shared_ptr<BulletinBoardParams> paramsFromCoordinator();

		std::shared_ptr<data::DataRepository> repo;

		log4cplus::Logger logger{logger::getDefaultLogger()};

		static void await(std::optional<CyclicBarrier> &barrier);

		std::mutex countermutex;
		std::thread counterWorker;
		std::mutex readyStatemutex;
		std::thread readyStateWorker;
		std::atomic<bool> shouldStopReaydCheck;
		std::atomic<bool> shouldStopCalc;
		std::atomic<bool> started;

		std::vector<CounterType> generateSequences(unsigned inBits, unsigned outBits) const;

	public:
		explicit BulletinBoard(std::weak_ptr<ICoordinatorForBulletinBoard> coordinator);

		BulletinBoard(std::weak_ptr<ICoordinatorForBulletinBoard> coordinator, unsigned int id);

		~BulletinBoard();

		// c = !(a & b)
		void bsNAND(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const;

		void bsNAND(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const;

		// c = !(a | b)
		void bsNOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const;

		void bsNOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const;

		// c = a & b
		void bsAND(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b) const;

		void bsAND(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b, std::optional<CyclicBarrier> &barrier) const;

		// c = !a & b
		void bsANDNY(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const;

		void bsANDNY(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const;

		// c = a & !b
		void bsANDYN(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const;

		void bsANDYN(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const;

		void bsXOR(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b) const;

		void bsXOR(MKLweSample &result, const MKLweSample &sample_a, const MKLweSample &sample_b, std::optional<CyclicBarrier> &barrier) const;

		void bsOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b) const;

		void bsOR(MKLweSample &result, MKLweSample const &sample_a, MKLweSample const &sample_b, std::optional<CyclicBarrier> &barrier) const;

		void initCounters();

		void enlargeCounters();

		std::vector<CounterType> const &getCounters();

		std::shared_ptr<BulletinBoardParams> const &getParams();

		CounterType expandVote(const CounterType &vote) const;

		void generateKeys() override;

		void submitVote(unsigned int id, CounterType vote) override;

		void voteDone() override;

		void bootstap(MKLweSample &out, Torus32 MU, const MKLweSample &in) const;

		std::shared_ptr<MKLweBootstrappingKey> getPtr() const;

		std::shared_ptr<MKLweBootstrappingKeyFFT> generateBsKeyFft() const;

		void sendPartialResults(std::vector<std::vector<Torus32>> vector) override;

		CounterType &addVote(CounterType &counter, std::shared_ptr<MKLweSample> const &vote);

		void checkBallotPaperStates();

		void processVotes();

		void startWorkers();

		std::vector<hsr::data::model::Vote> getVotes();

		std::vector<hsr::data::model::Vote> getVotesByHash(std::string hash);
	};
}
#endif /* TELLER_H_ */
