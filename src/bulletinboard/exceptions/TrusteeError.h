#ifndef MK_TFHE_VOTING_TRUSTEEERROR_H
#define MK_TFHE_VOTING_TRUSTEEERROR_H


#include <mk-tfhe-voting/exceptions/Exception.h>

struct TrusteeError : Exception {
	TrusteeError(const std::string &message, const std::exception &cause) : Exception(message, cause) {}

	explicit TrusteeError(const std::string &message) : Exception(message) {}
};


#endif //MK_TFHE_VOTING_TRUSTEEERROR_H
